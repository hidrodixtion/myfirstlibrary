package com.projectbox.validatoor

/**
 * Created by adinugroho
 */
interface IValidator {
    fun isValid(email: String): Boolean
}