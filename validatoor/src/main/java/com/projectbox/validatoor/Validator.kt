package com.projectbox.validatoor

/**
 * Created by adinugroho
 */
class Validator: IValidator {
    override fun isValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}