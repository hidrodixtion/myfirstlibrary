package com.projectbox.myfirstlibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.projectbox.validatoor.IValidator
import com.projectbox.validatoor.Validator

class MainActivity : AppCompatActivity() {
    private lateinit var validator: IValidator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        validator = Validator()
        Log.v("MainActivity", validator.isValid("hello@am.cc").toString())
    }
}